from clases import Visitante, lista_visitantes


def agregar_visitante():
    while True:
        nombre = input(" Nombre de visitante: ")
        edad = input(" Edad de visitante: ")
        altura = input(" Altura de visitante: ")
        opcion = input(" Deseas agregar nuevo visitante si/No :")
        new_visitante = Visitante(nombre, edad, altura)
        lista_visitantes.append(new_visitante)
        if(opcion == "no"):
            break
        
def enlistar_visitantes():
    for i ,visitante in enumerate(lista_visitantes):
        print(f"{ i + 1} NOMBRE: {visitante.nombre}, EDAD: {visitante.edad} anios, ALTURA: {visitante.altura} cm")
        
def modificar_visitante():
    while True:
        enlistar_visitantes()
        opcion = input("Selecciona el número del visitante que deseas modificar (o escribe 'salir' para volver al menú principal): ")
        if opcion.lower() == "salir":
            break   
        try:
            indice = int(opcion) - 1
            visitante = lista_visitantes[indice]
            nuevo_nombre = input(f'Ingrese el nuevo nombre para {visitante.nombre}: ')
            nueva_edad = input(f'Ingrese la nueva edad para {visitante.nombre} :')
            nueva_altura = input(f'ingrese nueva altura para {visitante.nombre} :')
            visitante.nombre = nuevo_nombre
            visitante.edad = int(nueva_edad)
            visitante.altura = int(nueva_altura)
            print("Visitante modificado con éxito")
        except (ValueError, IndexError):
            print('Opción no válida. Por favor, selecciona un número de visitante válido.')