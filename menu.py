from atraccion import agregar_atraccion, enlistar_atracciones, modificar_atraccion, procesar_cola_espera
from visitantes import agregar_visitante, enlistar_visitantes, modificar_visitante
from reserva import reserva, enlistar_reservas
from clases import lista_atracciones


while True:
    print("1. Agregar Atracciones")
    print("2. Modificar Atracciones")
    print("3. Agregar Visitantes")
    print("4. Modificar Visitantes")
    print("5. Enlistar Atracciones")
    print("6. Enlistar Visitantes")
    print("7. Reservas")
    print("8. Enlistar reservas")
    print("9. Simular Paso del Tiempo")
    print("10. Salir")

    opcion = input("Ingresa una opcion: ")

    if opcion == "1":
        agregar_atraccion()
    elif opcion == "2":
        modificar_atraccion()
    elif opcion == "3":
        agregar_visitante()
    elif opcion == "4":
        modificar_visitante()
    elif opcion == "5":
        enlistar_atracciones()
    elif opcion == "6":
        enlistar_visitantes()
    elif opcion == "7":
        reserva()
    elif opcion == "8":
        enlistar_reservas()
    elif opcion == "9":
        for atraccion in lista_atracciones:
            atraccion.actualizar_estado()
        procesar_cola_espera()
        print("Se ha simulado el paso del tiempo.")

    elif opcion == "10":
        print("Adios")
        break
    else:
        print("Opcion incorrecta, ingresa un número del 1 al 10.")