from clases import Atraccion, lista_atracciones

cola_espera = []  # Definir cola_espera como una variable global

def agregar_atraccion():
    while True:
        nombre = input('Ingrese nombre de la atracción: ')
        capacidad_maxima = input('Ingrese capacidad máxima de la atracción: ')
        tiempo_espera = input('Ingrese tiempo de espera: ')
        opciones = input('¿Deseas agregar otra atracción? (si/no): ')  
        new_atraccion = Atraccion(nombre, capacidad_maxima, tiempo_espera)
        lista_atracciones.append(new_atraccion)
        if opciones.lower() == 'no':
            break

def procesar_cola_espera():
    # Simula admitir nuevos visitantes de la cola de espera
    for atraccion in lista_atracciones:
        if len(atraccion.reservas_actuales) < atraccion.capacidad_maxima and cola_espera:
            visitante = cola_espera.pop(0)
            atraccion.reservas_actuales.append(visitante)
            print(f"{visitante.nombre} de la cola de espera fue admitido a {atraccion.nombre}.")

def enlistar_atracciones():
    for i, atraccion in enumerate(lista_atracciones):
        print(f"{i + 1}, NOMBRE: {atraccion.nombre}, CAPACIDAD MAXIMA: {atraccion.capacidad_maxima} Personas, TIEMPO DE ESPERA: {atraccion.tiempo_espera} Minutos")

def modificar_atraccion():
    while True:
        enlistar_atracciones()
        opcion = input("Selecciona el número de la atracción que deseas modificar (o escribe 'salir' para volver al menú principal): ")
        if opcion.lower() == "salir":
            break
        try: 
            indice = int(opcion) - 1
            atraccion = lista_atracciones[indice]
            nuevo_nombre = input(f'Ingrese el nuevo nombre para {atraccion.nombre}: ')
            nuevo_capacidad = input(f'Ingrese la nueva capacidad máxima para {atraccion.nombre}: ')
            nuevo_tiempo_espera = input(f'Ingrese el nuevo tiempo de espera para {atraccion.nombre}: ')
            atraccion.nombre = nuevo_nombre
            atraccion.capacidad_maxima = int(nuevo_capacidad)
            atraccion.tiempo_espera = int(nuevo_tiempo_espera)
            print("Atracción modificada con éxito")
        except (ValueError, IndexError):
                    print('Opción no válida. Por favor, selecciona un número de atracción válido.')