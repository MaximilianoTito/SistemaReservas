# Sistema de Reservas para un Parque de lista_atracciones
lista_atracciones = []
cola_espera = []
class Atraccion:
    def __init__(self, nombre, capacidad_maxima, tiempo_espera):
        self.nombre = nombre
        self.capacidad_maxima = int(capacidad_maxima)
        self.tiempo_espera = int(tiempo_espera)
        self.tiempo = 0
        self.reservas_actuales = []

    def actualizar_estado(self):
        # Simula la salida de los visitantes
        self.reservas_actuales = []
        self.tiempo += 1

    def agregar_reserva(self, visitante):
        if len(self.reservas_actuales) < self.capacidad_maxima:
            self.reservas_actuales.append(visitante)
            print(f"{visitante.nombre} reservó {self.nombre}.")
        else:
            print(f"{self.nombre} está lleno. {visitante.nombre} irá a la cola de espera.")

    def procesar_cola_espera(self, cola_espera):
        # Simula admitir nuevos visitantes de la cola de espera
        if len(self.reservas_actuales) < self.capacidad_maxima and cola_espera:
            visitante = cola_espera.pop(0)
            self.reservas_actuales.append(visitante)
            print(f"{visitante.nombre} de la cola de espera fue admitido a {self.nombre}.")
        
lista_visitantes = []
class Visitante:
    def __init__(self, nombre, edad, altura):
        self.nombre = nombre
        self.edad = int(edad)
        self.altura = int(altura)
    
lista_reservaciones = []
class Reserva:
    def __init__(self, atraccion, visitante):
        self.atraccion = atraccion 
        self.visitante = visitante 