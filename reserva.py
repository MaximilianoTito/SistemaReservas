from clases import Reserva, lista_reservaciones
from visitantes import lista_visitantes
from atraccion import lista_atracciones

from clases import Reserva, lista_reservaciones
from visitantes import lista_visitantes
from atraccion import lista_atracciones

def reserva():
    print('Visitantes Disponibles')
    for i, visitante in enumerate(lista_visitantes, start=1):
        print(f"{i}. {visitante.nombre}")

    opcion_visitante = int(input("Selecciona el número del visitante: "))
    visitante_seleccionado = lista_visitantes[opcion_visitante - 1]

    print("Atracciones disponibles")
    for i, atraccion in enumerate(lista_atracciones, start=1):
        print(f"{i}. {atraccion.nombre}")

    opcion_atraccion = int(input("Selecciona el número de la atracción: "))
    atraccion_seleccionada = lista_atracciones[opcion_atraccion - 1]

    atraccion_seleccionada.agregar_reserva(visitante_seleccionado)
    lista_reservaciones.append(Reserva(atraccion_seleccionada, visitante_seleccionado))
    
def enlistar_reservas():
    for i, reserva in enumerate(lista_reservaciones, start=1):
        nombre_atraccion = reserva.atraccion.nombre
        nombre_visitante = reserva.visitante.nombre
        print(f"{i}. Atracción: {nombre_atraccion}, Visitante: {nombre_visitante}")